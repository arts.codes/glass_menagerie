Thank you for purchasing "Realistic Plastic & Metal Substance Kit" Asset for Unity 3D!

You know have the power of an extremely realistic and customizable substance for Plastic, Metal, and Pearl materials. 

Main features:
- 4K Resolution (2K Supported by Unity 5)
- Customizable Color of plastic surface
- Customizable Metal map intensity
- Customizable Transparency
- Customizable Glossiness map intensity
- Customizable wet or dry surface
- Customizable inwards/outwards scratches

List of Sample Materials produced with the Substance:
- Black Pure Pearl
- Custom Scratched White Plastic
- Hard-Scratched Blue Plastic Inwards
- Hard-Scratched Green Plastic Outwards
- Medium-Scratched Azure Green Plastic Inwards
- Medium-Scratched Blue Plastic Outwards
- Medium-Scratched Light Violet Plastic Outwards
- Medium-Scratched Orange Plastic Outwards
- Medium-Scratched Red Plastic Inwards
- Metallic Azure Wet
- Metallic Blue
- Metallic Blue Wet
- Metallic Gray
- Metallic Gray Custom Scratched
- Metallic Gray Hard Scratched
- Metallic Gray Medium Scratched
- Metallic Gray Soft Scratched
- Metallic Gray Wet
- Metallic Light Green Wet
- Rough Blue Plastic
- Rough Blue Plastic Wet
- Rough Light Green Plastic Wet
- Scratched Black Wet
- Simple Blue Plastic
- Simple Green Plastic
- Simple Red Plastic
- Soft-Scratched Blue Plastic
- Soft-Scratched Blue Plastic Wet
- Wet Black Plastic
- Wet Blue Blue
- Wet Green Plastic
- Wet Red Plastic
- White Pure Pearl

SETUP SCENE:
1 - Go to Etid -> Project Settings -> Player
2 - Switch color space from Gamma to Linear (This will allow you to use HDR features and obtain more realistic results)


HOW TO USE THE SUBSTANCE:
To have a quick demo, navigate to "Scene" folder and open "Demo Box Sphere". 
In this scene, a simple cube block with a sphere over it will help you to try the different editable materials of the Substance.
Click on the Cube or the Sphere from the Hierarchy, and scroll to the material section.
You will note that, in addition to normal parameters, you will have more controls for:
- Plastic Color
- Plastic Transparency
- Metal Intensity
- Roughness Intensity
- Do scratch have a color?
- Scratches Color
- Are scratches inwards? (Yes = Inwards, No = Outwards)
- Scratches selection
- Is it a wet surface?
- Additional parameters for the generation of scratches of selection 3 and selection 6.

These parameters let you choose the perfect material properties you need for your project.
You can find a list of Sample materials configurations I created for you on Folder "Substance" selecting the Substance file "Substance Kit".

Feel free to subscribe and follow me on Twitter for news on my next projects:
@Aleregal90 

My personal website:
https://alessioregalbuto.com

Do you have requests for a new material you need for your project? Feel free to drop me a message:
alessioregalbuto@outlook.com 

Don't forget to rate my work to support my efforts and make me share more with the community!
Ratings are VERY IMPORTANT for the development of these Assets.

Please let me know what you think and if you want more!

Best Regards,
Alessio Regalbuto
